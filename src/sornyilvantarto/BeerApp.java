package sornyilvantarto;

import java.io.*;

public class BeerApp {
	private BeerCollection beers;
	private BufferedReader reader;

	public BeerApp() {
		this.beers = new BeerCollection();
		InputStream is = System.in;
		InputStreamReader isr = new InputStreamReader(is);
		this.reader = new BufferedReader(isr);
	}

	public void finalize() {
		try {
			this.reader.close();
		} catch (IOException e) {
			System.out.println("Could not close reader");
			e.printStackTrace();
		}
	}

	private boolean process(String[] cmd) {
		if (cmd != null && cmd.length >= 1) {
			if (cmd[0].equals("add")) {
				beers.add(cmd);
				return true;
			} else if (cmd[0].equals("list")) {
				beers.list(cmd);
				return true;
			} else if (cmd[0].equals("load")) {
				this.load(cmd);
				return true;
			} else if (cmd[0].equals("save")) {
				this.save(cmd);
				return true;
			} else if (cmd[0].equals("search")) {
				beers.search(cmd);
				return true;
			} else if (cmd[0].equals("find")) {
				beers.find(cmd);
				return true;
			} else if (cmd[0].equals("delete")) {
				beers.delete(cmd);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void load(String[] cmd) {
		if (cmd != null && cmd.length == 2) {
			try {
				FileInputStream f = new FileInputStream(cmd[1]);
				ObjectInputStream in = new ObjectInputStream(f);
				this.beers = (BeerCollection) in.readObject();
				in.close();
			} catch (IOException ex) {
				System.out.println("Something went wrong while loading beers.");
				ex.printStackTrace();
			} catch (ClassNotFoundException ex) {
				System.out.println("BeerCollection class not found while loading beers.");
				ex.printStackTrace();
			}
		}
	}

	private void save(String[] cmd) {
		if (cmd != null && cmd.length == 2) {
			try {
				FileOutputStream f = new FileOutputStream(cmd[1]);
				ObjectOutputStream out = new ObjectOutputStream(f);
				out.writeObject(this.beers);
				out.close();
			} catch (IOException ex) {
				System.out.println("Something went wrong while saving beers.");
				ex.printStackTrace();
			}
		}
	}

	public void run() {
		String[] cmd = null;
		while (true) {

			try {
				cmd = reader.readLine().split(" ");
			} catch (IOException e) {
				System.out.println("Something went wrong.");
				e.printStackTrace();
			}

			if (cmd != null && cmd.length > 0) {
				if (process(cmd)) {

				} else if (cmd[0].equals("exit")) {
					break;
				} else {
					System.out.println("Invalid command.");
				}
			} else {
				System.out.println("There no command to process.");
			}
		}
	}

}
