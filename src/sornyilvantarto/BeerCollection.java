package sornyilvantarto;

import java.io.Serializable;
import java.util.*;

public class BeerCollection implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<Beer> beers;

	private enum SortedBy {
		NOTHING, NAME, STYLE, STRENGTH
	}

	private SortedBy sortedBy;

	BeerCollection() {
		this.beers = new ArrayList<Beer>();
		this.sortedBy = SortedBy.NOTHING;
		// *
		beers.add(new Beer("Szilaj_Meggy", "lager", 4.0));
		beers.add(new Beer("Red_Ale", "ale", 4.8));
		// */
	}

	public void add(String[] cmd) {
		if (cmd != null && cmd.length == 4) {
			Beer beer = new Beer(cmd[1], cmd[2], Double.parseDouble(cmd[3]));
			beers.add(beer);
			this.sortedBy = SortedBy.NOTHING;
		} else {
			System.out.println("Invalid parameters.");
		}
	}

	public void list(String[] cmd) {
		if (cmd != null && cmd.length >= 1) {

			/* Sorting */
			for (int i = cmd.length - 1; i > 0; --i) {
				if (cmd[i].equals("name")) {
					this.beers.sort(new NameComparator());
					this.sortedBy = SortedBy.NAME;
				} else if (cmd[i].equals("style")) {
					this.beers.sort(new StyleComparator());
					this.sortedBy = SortedBy.STYLE;
				} else if (cmd[i].equals("strength")) {
					this.beers.sort(new StrengthComparator());
					this.sortedBy = SortedBy.STRENGTH;
				} else {
					System.out.println("Invalid parameters.");
					return;
				}
			}

			/* Listing */
			for (Beer beer : beers) {
				System.out.println(beer.toString());
			}
		} else {
			System.out.println("Invalid parameters.");
		}
	}

	public class NameComparator implements Comparator<Beer> {
		@Override
		public int compare(Beer b1, Beer b2) {
			return b1.getName().compareTo(b2.getName());
		}
	}

	public class StyleComparator implements Comparator<Beer> {
		@Override
		public int compare(Beer b1, Beer b2) {
			return b1.getStyle().compareTo(b2.getStyle());
		}
	}

	public class StrengthComparator implements Comparator<Beer> {
		@Override
		public int compare(Beer b1, Beer b2) {
			if (b1.getStrength() < b2.getStrength()) {
				return -1;
			} else if (b1.getStrength() > b2.getStrength()) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public void search(String[] cmd) {
		if (cmd != null) {
			if (cmd.length == 2) {
				this.beers.forEach((beer) -> this.printIfNameEquals(beer, cmd[1]));
			} else if (cmd.length == 3) {
				if (cmd[1].equals("name")) {
					this.beers.forEach((beer) -> this.printIfNameEquals(beer, cmd[2]));
				}
				else if (cmd[1].equals("style")) {
					this.beers.forEach((beer) -> this.printIfStyleEquals(beer, cmd[2]));
				}
				else if (cmd[1].equals("strength")) {
					this.beers.forEach((beer) -> this.printIfStrengthEquals(beer, Double.parseDouble(cmd[2])));
				} else {
					System.out.println("There is no search option called '" + cmd[1] + "'.");
				}
			} else {
				System.out.println("Invalid parameters.");
			}
		} else {
			System.out.println("Invalid parameters.");
		}
	}

	private void printIfNameEquals(Beer beer, String param) {
		if (beer.getName().equals(param)) {
			System.out.println(beer.toString());
		}
	}
	
	private void printIfStyleEquals(Beer beer, String param) {
		if (beer.getStyle().equals(param)) {
			System.out.println(beer.toString());
		}
	}
	
	private void printIfStrengthEquals(Beer beer, double param) {
		if (beer.getStrength() == param) {
			System.out.println(beer.toString());
		}
	}

	public void find(String[] cmd) {
		if (cmd != null) {
			if (cmd.length == 2) {
				this.beers.forEach((beer) -> this.printIfNameMatches(beer, cmd[1]));
			} else if (cmd.length == 3) {
				if (cmd[1].equals("name")) {
					this.beers.forEach((beer) -> this.printIfNameMatches(beer, cmd[2]));
				}
				else if (cmd[1].equals("style")) {
					this.beers.forEach((beer) -> this.printIfStyleMatches(beer, cmd[2]));
				}
				else if (cmd[1].equals("strength")) {
					this.beers.forEach((beer) -> this.printIfStronger(beer, Double.parseDouble(cmd[2])));
				}
				else if (cmd[1].equals("weaker")) {
					this.beers.forEach((beer) -> this.printIfWeaker(beer, Double.parseDouble(cmd[2])));
				} else {
					System.out.println("There is no find option called '" + cmd[1] + "'.");
				}
			} else {
				System.out.println("Invalid parameters.");
			}
		} else {
			System.out.println("Invalid parameters.");
		}
	}

	private void printIfNameMatches(Beer beer, String param) {
		if (beer.getName().matches(".*" + param + ".*")) {
			System.out.println(beer.toString());
		}
	}
	
	private void printIfStyleMatches(Beer beer, String param) {
		if (beer.getStyle().matches(".*" + param + ".*")) {
			System.out.println(beer.toString());
		}
	}
	
	private void printIfStronger(Beer beer, double param) {
		if (beer.getStrength() >= param) {
			System.out.println(beer.toString());
		}
	}
	
	private void printIfWeaker(Beer beer, double param) {
		if (beer.getStrength() <= param) {
			System.out.println(beer.toString());
		}
	}

	public void delete(String[] cmd) {
		if (cmd != null && cmd.length == 2) {
			Comparator<Beer> comparator = null;
			switch (this.sortedBy) {
			case NOTHING:
				comparator = new NameComparator();
				this.beers.sort(comparator);
				this.sortedBy = SortedBy.NAME;
				break;
			case NAME:
				comparator = new NameComparator();
				break;
			case STYLE:
				comparator = new StyleComparator();
				break;
			case STRENGTH:
				comparator = new StrengthComparator();
				break;
			}

			int index = Collections.binarySearch(this.beers, new Beer(cmd[1], null, 0), comparator);
			if (index >= 0 && index < beers.size()) {
				this.beers.remove(index);
			} else {
				System.out.println("Beer '" + cmd[1] + "' not found.");
			}
		} else {
			System.out.println("Invalid parameters.");
		}
	}
}
